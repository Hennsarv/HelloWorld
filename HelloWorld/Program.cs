﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HelloWorld
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Hello World!");

            char a = (char)372;  // 
            Console.WriteLine(a);

            string filename = @"C:\Test";

            string Tere = "Tere ilus poiss Henn" + System.Environment.NewLine +  "mina olen arvuti";

            Console.WriteLine(Tere);

            string eesnimi = "Henn";
            string perenimi = "Sarv";
            string nimi = eesnimi;
            nimi += " ";
            nimi += perenimi;

            Console.WriteLine(nimi);

            int vanus = 62;

            string x = String.Format("Inimene eesnimega {0} ja perenimega {1}", eesnimi, perenimi);

            Console.WriteLine(x);

            Console.WriteLine("Inimene {0} {1} on {2} aastat vana", eesnimi, perenimi, vanus);

            Console.WriteLine($"Inimene {eesnimi} {perenimi} on {vanus} aastat vana");

            // näide kohahoidjatest

            string eesti = "Inimene {0} {1} on {2} aastat vana";
            string inglise = "Persons {1}, {0} age is {2}";

            Console.Write("mis keelt sa tahad (i inglise, e eesti): ");
            string keel = Console.ReadLine();
            string vastus = String.Format(keel == "i" ? inglise : eesti, eesnimi, perenimi, vanus);

            Console.WriteLine(vastus);

        }
    }
}
